# frozen_string_literal: true

# db/migrate/20201031174811_create_destination_addresses.rb
class CreateDestinationAddresses < ActiveRecord::Migration[6.0]
  def change
    create_table :destination_addresses do |t|
      t.string :destination_ip, null: false
      t.string :destination_owner, null: false

      t.timestamps
    end

    add_index :destination_addresses, [:destination_ip], unique: true
  end
end
