FROM phusion/baseimage:18.04-1.0.0

# Command "docker run" can override this variables
ENV TZ=Europe/Warsaw
ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8

# Set correct environment variables.
ENV HOME /app

RUN mkdir -p /app
RUN apt-get update && apt-get install -y \
  wget \
  less \
  curl \
  build-essential \
  libpq-dev \
  git \
  netcat \
  libsnappy-dev \
  htop \
  zlib1g-dev \
  tzdata \
  iputils-ping \
  libxml2-dev \
  libxslt-dev \
  nodejs \
  yarn \
  shared-mime-info \
  nmap \
  geoip-database-extra \
  geoip-bin

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

###### INSTALL NGINX  #####

ADD docker/config/nginx.list /etc/apt/sources.list.d/nginx.list

RUN wget http://nginx.org/packages/keys/nginx_signing.key

RUN cat nginx_signing.key | apt-key add -

RUN apt-get update && apt-get install -y nginx

# Remove unnecessery config
RUN rm /etc/nginx/conf.d/default.conf

# Add the nginx config
ADD docker/config/nginx.conf /etc/nginx/nginx.conf

###### INSTALL RUBY 2.6 #############

RUN apt-add-repository ppa:brightbox/ruby-ng && apt-get update && apt-get install -y ruby2.6 ruby2.6-dev

RUN apt-get clean && rm -rf /var/lib/apt/lists/*

RUN mkdir /app/tmp
RUN mkdir /app/tmp/pids
RUN mkdir /app/storage

# This sets the context of where commands will be ran in and is documented
# on Docker's website extensively.
WORKDIR /app

# Ensure gems are cached and only get updated when they change. This will
# drastically increase build times when your gems do not change.
COPY Gemfile Gemfile
COPY Gemfile.lock Gemfile.lock
RUN gem install bundler
RUN gem install nokogiri --platform=ruby
RUN bundle install

#### ADD INIT SERVICES ######
RUN mkdir /etc/service/puma
COPY docker/config/init/puma.sh /etc/service/puma/run
RUN chmod +x /etc/service/puma/run

RUN mkdir /etc/service/nginx
COPY docker/config/init/nginx.sh /etc/service/nginx/run
RUN chmod +x /etc/service/nginx/run

RUN mkdir /etc/service/sidekiq
COPY docker/config/init/sidekiq.sh /etc/service/sidekiq/run
RUN chmod +x /etc/service/sidekiq/run

# Add the Rails app
ADD . /app

EXPOSE 8080
