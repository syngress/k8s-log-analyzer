# frozen_string_literal: true

module Menus
  # /app/serializers/menus/menu_serializer.rb
  class MenuSerializer
    include FastJsonapi::ObjectSerializer

    attributes :id, :restaurant_id, :category, :name, :description, :price, :currency, :created_at, :updated_at
  end
end
