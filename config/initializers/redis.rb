SIDEKIQ_REDIS_SETTINGS = if Settings.redis.sidekiq.sentinels?
  {
    url: "redis://#{Settings.redis.sidekiq.master_name}",
    role: :master,
    db: Settings.redis.sidekiq.db,
    sentinels: Settings.redis.sidekiq.sentinels.map!(&:to_h)
  }
else
  {
    host: Settings.redis.sidekiq.host,
    port: Settings.redis.sidekiq.port,
    db: Settings.redis.sidekiq.db
  }
end

CACHE_REDIS_SETTINGS = if Settings.redis.cache.sentinels?
  {
    url: "redis://#{Settings.redis.cache.master_name}",
    role: :master,
    db: Settings.redis.cache.db,
    sentinels: Settings.redis.cache.sentinels.map!(&:to_h)
  }
else
  {
    host: Settings.redis.cache.host,
    port: Settings.redis.cache.port,
    db: Settings.redis.cache.db
  }
end

Rails.application.config.cache_store = :redis_store, CACHE_REDIS_SETTINGS.merge(namespace: :cache, expires_in: Settings.cache.default.expires_in)
Rails.cache = ActiveSupport::Cache.lookup_store(Rails.application.config.cache_store)

REDIS_CACHE   = Redis.new(CACHE_REDIS_SETTINGS)
REDIS_SIDEKIQ = Redis.new(SIDEKIQ_REDIS_SETTINGS)
