# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.6'

gem 'bcrypt', '~> 3.1.16'
gem 'concord', '~> 0.1.6'
gem 'config', '~> 2.2.1'
gem 'csv', '~> 3.1.7'
gem 'dry-schema', '~> 1.5.5'
gem 'dry-struct', '~> 1.3.0'
gem 'dry-types', '~> 1.4.0'
gem 'dry-validation', '~> 1.5.6'
gem 'jbuilder', '~> 2.10.1'
gem 'jsonapi-serializer', '~> 2.1.0'
gem 'kaminari', '~> 1.2.1'
gem 'pg', '~> 1.2.3'
gem 'puma', '~> 5.1.1'
gem 'rack-cors', '~> 1.1.1'
gem 'rack-protection', '~> 2.1.0'
gem 'rails', '6.1.3.1'
gem 'redis-rails', '~> 5.0'
gem 'rest-client', '~> 2.1'
gem 'sidekiq', '~> 6.1.2'
gem 'sidekiq-cron', '~> 1.2'
gem 'sidekiq-monitor-stats', '~> 0.0.4'
gem 'rake', '~> 13.0'
gem 'mimemagic', '~> 0.4'
gem 'marcel', '~> 1.0'

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'rubocop', '~> 1.1.0'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  gem 'database_cleaner', '~> 1.8.5'
  gem 'factory_bot_rails', '~> 6.1.0'
  gem 'faker', '~> 2.14.0'
  gem 'rspec-rails', '~> 4.0.1'
  gem 'shoulda-matchers', '~> 4.4.1'
  gem 'webmock', '~> 3.9.1'
end
