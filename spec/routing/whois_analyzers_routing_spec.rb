# frozen_string_literal: true

require 'rails_helper'

RSpec.describe WhoisReportsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/whois_reports').to route_to('whois_reports#index')
    end

    it 'routes to #show' do
      expect(get: '/whois_reports/1').to route_to('whois_reports#show', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/whois_reports').to route_to('whois_reports#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/whois_reports/1').to route_to('whois_reports#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/whois_reports/1').to route_to('whois_reports#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/whois_reports/1').to route_to('whois_reports#destroy', id: '1')
    end
  end
end
