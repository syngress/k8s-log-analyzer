# frozen_string_literal: true

FactoryBot.define do
  factory :whois_report do
    ip { 'MyString' }
    whois { 'MyString' }
  end
end
