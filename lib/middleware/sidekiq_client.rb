module Middleware
  class SidekiqClient

    def call(worker, items, queue, redis_pool = nil)
      items[:request_id] = Thread.current[:request_id] if Thread.current[:request_id].present?
      yield
    end
  end
end
