module Middleware
  class SidekiqServer

    def call(worker, items, queue)
      Thread.current[:request_id] = items['request_id']
      yield
    end
  end
end
