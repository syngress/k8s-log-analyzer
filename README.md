# LOG ANALYZER MICROSERVICE  
This project is part of **[K8S Log Server](https://bitbucket.org/syngress/k8s-log-server/src/master)**  

![RubyIMG](https://syngress.pl/images/ruby_logo.png)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

Things you may want to cover for local development:

* Ruby  
  `2.6.6`

* Rails  
  `6.1.3.1`  

* System dependencies and Services (job queues, cache servers, search engines, etc.)  
  `Redis 5.0.3`  
  `Postgres 1.2.3`  
  `Puma 5.0.2`   
  `Docker 3.3.1`

* Configuration  
  For OSX install packages from brew:  
  - `brew install nmap`
  - `brew install geoip`
  - `brew install shared-mime-info`

  For Linux use snap install geoip-lookup and nmap  

* Database creation and initialization  
  `rake db:create`  
  `rake db:migrate`  

* Requirements  
  `JSON API`  
  `Use dry stack (Schema, Struck, Types)`  
  `Use Netflix fast JsonApi serializers`  
  `GeoIPASNum database from https://www.maxmind.com`  

Based on data contained in the log file, application will generate daily CSV report.  
Logs are generated with a daily time interval.  
We will verify IP Address Ownership/Name of the original organization or individual name of the IP owner to which the IP address belongs.  

By giving specific date in the form `YYYY: MM: DD`, application will take a few steps in succession:  
- service filters all private addresses without processing them
- for each processed IP address, we collect the Oownership name  
- each public address (ipv4 / ipv6) with which the connection was made is saved with the ownership name to the database  
- reprocessing same public IP results in an update in the database  
- process is performed once a day based on the previous day's log file  
- process is performed in the background sidekiq queues   
- from the configuration level, we can assign hardware names to our local source_ip  

Public IP's will be translated using geoiplookup and store in Postgres database.    
Application allows us to analyze established connections.  

Service use simple bash onliner for this purpose.    

```code
geoiplookup -f /path/to/GeoIPASNum.dat `nmap -q -sL -n "#{destination_ip}" | grep "Nmap scan report for" | awk '{print $NF}' | head -1` | sed 's/GeoIP ASNum Edition: //'
```

Application will be written in Ruby, using Rails framework.  
Application configuration will contain device names associated to sourceIP address (assigns names to specific devices connected to your private network).  
CSV file will contain following information, ex:  

```code
SourceIP,HardwareName,DestinationIP,DestinationName,ConnectionTime
192.168.2.111,hardware_one,17.212.108.125,AS714 APPLE-ENGINEERING,Oct 24 14:42:58
192.168.2.111,hardware_one,17.253.108.125,AS6185 APPLE-AUSTIN,Oct 24 14:42:58
10.0.1.101,hardware_three,17.253.108.125,AS6185 APPLE-AUSTIN,Oct 26 17:40:22
192.168.2.111,hardware_one,23.197.105.233,AS16625 AKAMAI-AS,Nov 1 00:01:00
```  

Request allows us to send parameter that convert csv to xml.  
We will use `xslt` library for this purpose, conversion will be done in a separate microservice written in Java (Spring Platform).  
Communication between applications will be provided by Apache Kafka, as high-performance, low-latency, scalable event source.  

Due to the large amount of data to be processed (log from one day can contain from 100k to several million lines), report will be generated in sidekiq queues.  
Application will confirm report generation by email.  
Email address will be required as one of the request parameters. Functionality can be disabled in the application configuration.  
Depending on system resourceswe we will be able to decide how many simultaneous processes application can perform.  

# Generating a csv report  

Our application save network connections logs to the following directories.  
Each of the directories defines with its name a day on which the logs were generated by network devices.  

![LogAnalyzerLogs](https://syngress.pl/images/kubernetes_log_server/LogAnalyzerLogs.png)  

Daily network connection report is stored as one `message` file.  
By sending POST request, application get two attributes from payload:  

- `log_date` pointing to the log directory.  
- `report_email` e-mail address to which report will be sent in the form of a `*.csv` file.  

From the selected directory, application will search for a `message` file to be analyzed line by line.  
Report is saved in the same directory as `*.csv` file.  
Report is sent to the selected email address.  

![LogAnalyzerLogsDir](https://syngress.pl/images/kubernetes_log_server/LogAnalyzerLogsDir.png)  

# Table of endpoint

WHOIS REPORTS  

|Method          |Endpoint                                  |Note                            |
|----------------|------------------------------------------|--------------------------------|
|POST			       |`/whois_reports`            	            |Create CSV report from log file |
|GET             |`/whois_reports`                          |Get CSV reports collection      |
|GET			       |`/whois_reports/:id`                      |Get CSV report object  	       |

# Sequence Diagram

![LogAnalyzerUML](https://syngress.pl/images/kubernetes_log_server/LogAnalyzerUML.png)  


**Free Software, Hell Yeah!**  

**THIS PROJECT IS STILL UNDER CONSTRUCTION**
