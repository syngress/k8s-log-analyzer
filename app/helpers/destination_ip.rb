# frozen_string_literal: true

# app/helpers/destination_ip.rb
module DestinationIp
  def self.lookup_v4(destination_ip)
    %x(geoiplookup -f #{Settings.geoip_file_path} `nmap -q -sL -n "#{destination_ip}" | grep "Nmap scan report for" | awk '{print $NF}' | head -1` | sed 's/GeoIP ASNum Edition: //')
  end

  def self.lookup_v6(destination_ip)
    %x(geoiplookup -f #{Settings.geoip_file_path} `nmap -q -sL -6 "#{destination_ip}" | grep "Nmap scan report for" | awk '{print $(NF-1),"\t"}' | head -1` | sed 's/GeoIP ASNum Edition: //')
  end
end
