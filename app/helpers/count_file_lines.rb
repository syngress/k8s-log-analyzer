# frozen_string_literal: true

# app/helpers/count_file_lines.rb
module CountFileLines
  def self.proceed(file_path)
    csv_file?(file_path) ? File.read(file_path).each_line.count - 1 : File.read(file_path).each_line.count
  end

  def self.csv_file?(file_path)
    # Remove CSV header row from count
    File.extname(file_path) == '.csv'
  end
end
