# frozen_string_literal: true

json.array! @whois_reports, partial: 'whois_reports/whois_report', as: :whois_report
