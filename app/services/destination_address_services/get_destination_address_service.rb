# frozen_string_literal: true

module DestinationAddressServices
  # app/services/destination_address_services/get_destination_address_service.rb
  class GetDestinationAddressService
    include Service

    def initialize(attrs = {})
      @sort_by = attrs.fetch(:sort_by)
      @sort_order = attrs.fetch(:sort_order)
      @page = attrs.fetch(:page)
      @per_page = attrs.fetch(:per_page)
      @params = attrs.fetch(:params)
    end

    def call
      params['destination_ip'] ? destination_address : all_destination_addresses
    end

    private

    attr_reader :sort_by, :sort_order, :page, :per_page, :params

    def destination_address
      DestinationAddress.where(destination_ip: params['destination_ip'])
    end

    def all_destination_addresses
      DestinationAddress.all.order(order).page(page).per(per_page)
    end

    def order
      { sort_by => sort_order }
    end
  end
end
