# frozen_string_literal: true

module DestinationAddressServices
  # app/services/destination_address_services/create_destination_address_service.rb
  class CreateDestinationAddressService
    include Service

    def initialize(attrs = {})
      @form = attrs.fetch(:form)
    end

    def call
      form.errors?
      return build_result(validation_error) unless form.valid?

      build_result(destination_address)
    end

    private

    attr_reader :form

    def process_data(form)
      destination_ip = form.destination_ip
      destination_owner = lookup(destination_ip).strip

      {
        destination_ip: destination_ip,
        destination_owner: destination_owner
      }
    end

    def destination_address
      @destination_address ||= DestinationAddress.where(destination_ip: form.destination_ip)
                                                 .first_or_create(process_data(form))
    end

    def lookup(ip)
      if IpProtocolValidator.v4?(ip)
        DestinationIp.lookup_v4(ip).strip
      elsif IpProtocolValidator.v6?(ip)
        DestinationIp.lookup_v6(ip).strip
      else
        'unprocessable ip'
      end
    end
  end
end
