# frozen_string_literal: true

# app/services/process_update_params.rb
class ProcessUpdateParams
  include Service

  attr_reader :object, :params, :params_to_update, :update_params

  def initialize(attrs = {})
    @object = attrs.fetch :object
    @params = attrs.fetch :params
    @update_params = attrs.fetch(:update_params, {})
    @params_to_update = {}
  end

  def call
    perform_filter
    update_existing_params
    params_to_update
  end

  def perform_filter
    params.each do |key, value|
      if !object.respond_to?(key)
        params_to_update[key] = value
      elsif object.send(key) != value
        params_to_update[key] = value
      end
    end
  end

  def update_existing_params
    update_params.each do |key, value|
      params_to_update[key] = value unless params_to_update.key?(key.to_s)
    end
  end
end
