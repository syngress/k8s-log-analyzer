# frozen_string_literal: true

module DestinationAddresses
  # log_analyzer/app/params_filters/destination_addresses/params_for_sort.rb
  class ParamsForSort
    extend ParamsFilter

    class << self
      def allowed_attributes
        %i[sort_by sort_order]
      end

      def allowed_values
        %i[id destination_ip]
      end
    end
  end
end
