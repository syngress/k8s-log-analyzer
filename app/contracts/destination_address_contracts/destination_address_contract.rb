# frozen_string_literal: true

module DestinationAddressContracts
  # app/contracts/destination_address_contracts/destination_address_contract.rb
  class DestinationAddressContract < Dry::Validation::Contract
    schema do
      optional(:id)
      required(:destination_ip).filled(:string)
      optional(:destination_owner).filled(:string)
    end

    rule(:id, :destination_ip) do
      key.failure(I18n.t('errors.attributes.ip_address.invalid')) unless ipv4.match?(values[:destination_ip]) || ipv6.match?(values[:destination_ip])
    end

    def ipv4
      /#{Settings.patterns.ipv4_address}/
    end

    def ipv6
      /#{Settings.patterns.ipv6_address}/
    end
  end
end
