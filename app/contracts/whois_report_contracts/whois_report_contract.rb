# frozen_string_literal: true

module WhoisReportContracts
  # log_analyzer/app/contracts/whois_report_contracts/whois_report_contract.rb
  class WhoisReportContract < Dry::Validation::Contract
    schema do
      optional(:id)
      required(:log_date).filled(:string)
      required(:log_lines).filled(:integer)
      required(:execution_time).filled(:string)
      required(:report_email).filled(:string)
    end

    rule(:id, :log_date, :log_lines, :execution_time, :report_email) do
      key.failure(I18n.t('errors.attributes.log_date.invalid')) unless log_date_format.match?(values[:log_date])
      key.failure(I18n.t('errors.attributes.email.invalid')) unless email_format.match?(values[:report_email])
    end

    def log_date_format
      /#{Settings.patterns.log_date}/
    end

    def email_format
      /#{Settings.patterns.email}/
    end
  end
end
