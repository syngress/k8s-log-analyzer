# frozen_string_literal: true

# app/workers/whois_workers
module WhoisWorkers
  # app/workers/whois_workers/whois_worker.rb
  class WhoisWorker
    include Sidekiq::Worker

    PRIVATE_IPS = [
      IPAddr.new('10.0.0.0/8'),
      IPAddr.new('172.16.0.0/12'),
      IPAddr.new('192.168.0.0/16')
    ].freeze

    def perform(file_path, object_id, file_name, report_email)
      starting_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
      string_marker_original = 'ORIG:'
      string_marker_protocol = 'PROTO'
      file = open_file(file_path)
      worker_process_id = generate_random_id
      worker_data = []

      Rails.logger.info "= = = start whois_worker process #{worker_process_id} = = ="

      ActiveRecord::Base.logger.silence do
        file.each_line do |line|
          new_connection_ips = line[/#{string_marker_original}(.*?)#{string_marker_protocol}/m, 1]

          next unless new_connection_ips

          connection_date = line.split.first(3).join(' ')
          source_ip = new_connection_ips.scan(/SRC=([^\s]+)/).first
          destination_ip = new_connection_ips.scan(/DST=([^\s]+)/).first

          next if private_ip?(destination_ip)

          destination_owner = lookup(destination_ip[0])

          worker_data << [source_ip[0], hardware_name(source_ip), destination_ip[0], destination_owner, connection_date]
        end
      end

      CsvGeneratorService::GenerateCsv.new.call(worker_data, file_name) if Settings.csv_report.enable

      ending_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
      execution_time = update_execution_time(starting_time, ending_time, object_id)

      send_csv_report_email(execution_time, file_name, report_email)
      process_with_csv_parser(file_name)

      Rails.logger.info "== whois_worker process completed #{worker_process_id} =="
    rescue StandardError => e
      raise Error::InternalServerError, e
    end

    def update_execution_time(starting_time, ending_time, object_id)
      t = (ending_time - starting_time).round(0)
      mm, ss = t.divmod(60)
      hh, mm = mm.divmod(60)
      dd, hh = hh.divmod(24)

      process_execution_time = format('%<days>d days, %<hours>d hours, %<minutes>d minutes and %<seconds>d seconds', days: dd, hours: hh, minutes: mm, seconds: ss)

      WhoisReport.find_by(id: object_id).update_attribute(:execution_time, process_execution_time)
      process_execution_time
    end

    def open_file(file_path)
      File.open(file_path, 'r')
    rescue Errno::ENOENT => e
      raise Error::ResourceNotFound, e
    end

    def lookup(ip)
      if IpProtocolValidator.v4?(ip)
        DestinationIp.lookup_v4(ip).strip
      elsif IpProtocolValidator.v6?(ip)
        DestinationIp.lookup_v6(ip).strip
      else
        'unprocessable ip'
      end
    end

    def private_ip?(destination_ip)
      IpProtocolValidator.private_ip?(destination_ip[0])
    end

    def hardware_name(source_ip)
      hardware = Settings.source_ips.to_h
      source_ip_hardware = hardware.key(source_ip[0]).to_s
      source_ip_hardware.empty? ? 'unknown_hardware' : source_ip_hardware
    end

    def send_csv_report_email(execution_time, file_name, report_email)
      MessageWorkers::EmailWorker.perform_async(report_email, execution_time, file_name)
    end

    def generate_random_id
      SecureRandom.hex 32
    end

    def process_with_csv_parser(file_name)
      return unless Settings.csv_report.enable && Settings.csv_parser.enable

      begin
        csv_file = "#{file_name}.csv"
        RestClient.post Settings.csv_parser.uri, { log_file_name: csv_file, log_lines: count_csv_lines(csv_file) }.to_json, { content_type: :json, accept: :json }
      rescue RestClient::ExceptionWithResponse => e
        raise Error::InternalServerError, e
      end
    end

    def count_csv_lines(csv_file)
      @count_csv_lines ||= CountFileLines.proceed("#{Settings.csv_reports_path}/#{csv_file}")
    end
  end
end
