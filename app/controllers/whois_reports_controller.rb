# frozen_string_literal: true

# log_analyzer/app/controllers/whois_reports_controller.rb
class WhoisReportsController < ApplicationController
  before_action :whois_report, only: %i[show]
  include Pagination
  include Sorting

  def show
    render json: {
      serializer: WhoisReportSerializers::WhoisReportSerializer.new(
        whois_report
      ).serializable_hash
    }
  end

  def index
    setup_pagination(page: params[:page], per_page: params[:per_page])
    setup_sorting(sort_by: params[:sort_by], sort_order: params[:sort_order], params_for_sort: params_for_sort)
    render json: {
      recordsTotal: whois_reports.count,
      restaurants: WhoisReportSerializers::WhoisReportSerializer.new(
        whois_reports
      ).serializable_hash
    }
  end

  def create
    form = WhoisReportForms::CreateWhoisReportForm.new(whois_report_params)
    result = WhoisReportServices::CreateWhoisReportService.call(form: form)

    if result.error?
      render_error(result.object)
    else
      render json: WhoisReportSerializers::WhoisReportSerializer.new(result.object).serializable_hash.to_json
    end
  end

  private

  def whois_report
    @whois_report ||= WhoisReport.find(params[:id])
  rescue ActiveRecord::RecordNotFound => e
    raise Error::ResourceNotFound, e
  end

  def whois_reports
    @whois_reports ||= WhoisReportServices::GetWhoisReportService.call(
      page: page,
      per_page: per_page,
      sort_by: sort_by,
      sort_order: sort_order,
      params: params
    )
  end

  def whois_report_params
    params.permit(:log_date, :log_lines, :execution_time, :report_email).to_h.symbolize_keys
  end

  def params_for_sort
    ::WhoisReports::ParamsForSort.allowed_values
  end
end
