# frozen_string_literal: true

module WhoisReportForms
  # log_analyzer/app/forms/whois_report_forms/create_whois_report_form.rb
  class CreateWhoisReportForm < Dry::Struct
    # dry types
    module Types
      include Dry::Types(default: :nominal)
    end

    attribute :log_date, Types::String
    attribute :log_lines, Types::Integer.default(0)
    attribute :execution_time, Types::String.default('counting in progress')
    attribute :report_email, Types::String
    
    def errors?
      process_error if schema.errors.present?
    end

    def valid?
      schema.errors.blank?
    end

    def schema
      @schema ||= WhoisReportContracts::WhoisReportContract.new.call(to_hash)
    end

    def process_error
      return unless schema.errors.messages.first.predicate

      result_data = schema.errors.to_h
      raise ::Error::InvalidParameter.new result_data.keys.first.to_s, value: schema[:log_date], error: result_data.values.flatten.first
    end
  end
end
