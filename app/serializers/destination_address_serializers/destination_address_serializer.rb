# frozen_string_literal: true

module DestinationAddressSerializers
  # app/serializers/destination_address_serializers/destination_address_serializer.rb
  class DestinationAddressSerializer
    include JSONAPI::Serializer

    attributes :id, :destination_ip, :destination_owner, :created_at, :updated_at
  end
end
