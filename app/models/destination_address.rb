# frozen_string_literal: true

# app/models/destination_address.rb
class DestinationAddress < ApplicationRecord
  validates_presence_of :destination_ip, :destination_owner
end
