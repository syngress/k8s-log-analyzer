# frozen_string_literal: true

class Error
  # /app/models/error/resource_not_found.rb
  class ResourceNotFound < Error
    def initialize(error = nil)
      message = I18n.t :resource_not_found, scope: 'errors.messages'
      super(message, :resource_not_found, { 'error': error || '' }, Http::Status.new(404))
    end
  end
end
