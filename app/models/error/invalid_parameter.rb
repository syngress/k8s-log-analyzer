# frozen_string_literal: true

class Error
  # /app/models/error/invalid_parameter.rb
  class InvalidParameter < Error
    def initialize(parameter, options = {})
      value_message = options.key?(:value) ? options[:value].to_s : nil
      message = I18n.t :invalid_parameter, scope: 'errors.messages', value: value_message, parameter: parameter
      super(message, :invalid_parameter, { message: options[:error] }, Http::Status.new(400))
    end
  end
end
