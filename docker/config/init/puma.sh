#!/bin/sh
if [ "$APP_SERVER_DISABLED" != "1" ] ; then
  sh -c 'cd /app && bundle exec puma -C config/puma.rb'
fi
