#!/bin/sh

if [ "$SIDEKIQ_DISABLED" != "1" ] ; then
  sh -c 'cd /app && bundle exec sidekiq -q default,1 -t 20 -c $SIDEKIQ_THREADS -e $RAILS_ENV > /dev/null'
fi
